//============================================================================
// Name        : Robota.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <math.h>

#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#include <windows.h>
#include <GL/glut.h>
using namespace std;

typedef long Int32_t;
typedef long Int32;

#define SCREEN_HEIGHT 64
#define SCREEN_WIDTH  64


struct cRGB
{
	cRGB():r(1),g(1),b(1){}
	cRGB(float r , float g , float b):r(r),g(g),b(b){}
	float r,g,b;
};


struct Pixel
{
	Pixel(float _x=0,float _y=0):x(_x),y(_x)
	{
	}

	float x;
	float y;

	cRGB  mRGB;
	Int32 color;
};




void RenderString(Int32 x, Int32 y , const char* string, cRGB const& rgb)
{
	glColor3f(rgb.r, rgb.g, rgb.b);
	glRasterPos2i(x, y);

	int l=strlen( string );
	for(int i=0; i < l; i++) // loop until i is greater then l
	{
		glutBitmapCharacter( GLUT_BITMAP_TIMES_ROMAN_24, string[i]); // Print a character on the screen
	}
}



// Simulated frame buffer
Pixel GridCoords[SCREEN_HEIGHT][SCREEN_WIDTH];
Int32 Screen[SCREEN_HEIGHT][SCREEN_WIDTH];

struct Vertex
{


   Vertex(Int32 _x=0 , Int32_t _y=0, Int32 _color=0):
   x(_x) , y(_y) ,color(_color)
   {
   }

   Int32_t x,y;
   Int32 color;

   cRGB mRGB;

   /************  operator algebra C*  *********************/
   Vertex operator+( const Vertex& vector2)  const
   {
	   Vertex vector1(*this);
       return Vertex(vector1.x + vector2.x,
       		         vector1.y + vector2.y);
   }

   Vertex operator-(const Vertex& vector2)  const
   {
	   Vertex vector1(*this);
       return Vertex(vector1.x - vector2.x,
       		         vector1.y - vector2.y);
   }

   Vertex operator-()  const
   {
	   Vertex vector(*this);
       return Vertex(-vector.x, -vector.y);
   }

   Vertex operator * (const Int32& scalar)  const
   {
	   Vertex vector1(*this);
       return Vertex(vector1.x * scalar,
       		         vector1.y * scalar);
   }

   Vertex operator / (const Int32& scalar)  const
   {
	   Vertex vector1(*this);
       return Vertex(vector1.x / scalar,
       		         vector1.y / scalar);
   }

   Int32 dot(const Vertex& vector2)  const
   {
	    Vertex vector1(*this);
       return (vector1.x*vector2.x +
       		   vector1.y*vector2.y);
   }

   Int32 cross(const Vertex& vector2) const
   {
	 Vertex vector1(*this);
   	 return (vector1.x * vector2.y -
   	         vector2.x * vector1.y);
   }

   Int32 length() const
   {
       return sqrt(x*x + y*y);
   }

   /******************************************/

};

struct Line
{
	Line(Vertex _a , Vertex _b):a(_a),b(_b)
	{
	}

	Vertex a;
	Vertex b;

	/********* maths-analysis function ***********/

	Int32 distance(Vertex a, Vertex b, Vertex c)
	{
	  Int32 dx = a.x - b.x;
	  Int32 dy = a.y - b.y;
	  Int32 D = dx * (c.y - a.y) - dy * (c.x - a.x);
	  return (D / sqrt(dx * dx + dy * dy));
	}


	Vertex projectPointToLine(const Vertex &move)
	{
		Vertex line = a - b;
		Vertex dir( -line.y , line.x );

		Int32 lenghtLine = line.length() + 0.5;
		Int32 distationProject = (distance(a,b,move));

		return move - dir * distationProject / lenghtLine;
	}

	/******************************************/
};



struct Triangle
{
	Triangle(const Vertex& a ,
			 const Vertex& b ,
			 const Vertex& c):
	A(a) , B(b) , C(c)
	{

	}

	Vertex A;
	Vertex B;
	Vertex C;

	/******************************************/

	void InterpolationRGBColor( const Vertex &Move  , cRGB &rgb )
	{
		Line line1(B , C);
		Line line2(C , A);
		Line line3(B , A);

		Vertex derivativeA = line1.projectPointToLine(A);
		Vertex derivativeB = line2.projectPointToLine(B);
		Vertex derivativeC = line3.projectPointToLine(C);

		//Barycentric coordinate Axis
		Vertex BarycentricAxisA = (A - derivativeA);
		Vertex BarycentricAxisB = (B - derivativeB);
		Vertex BarycentricAxisC = (C - derivativeC);

		// Interpolation Tri
		Int32 p1_move = (derivativeA - Move).dot(BarycentricAxisA); // BarycentricAxisA.length();
		Int32 p2_move = (derivativeB - Move).dot(BarycentricAxisB); // BarycentricAxisB.length();
		Int32 p3_move = (derivativeC - Move).dot(BarycentricAxisC); // BarycentricAxisC.length();

		Int32 p1_magnitudeAxis = (derivativeA - A).dot(BarycentricAxisA); // BarycentricAxisA.length();
		Int32 p2_magnitudeAxis = (derivativeB - B).dot(BarycentricAxisB); // BarycentricAxisB.length();
		Int32 p3_magnitudeAxis = (derivativeC - C).dot(BarycentricAxisC); // BarycentricAxisC.length();


		float p1 = float(p1_move) / float(p1_magnitudeAxis);
		float p2 = float(p2_move) / float(p2_magnitudeAxis);
		float p3 = float(p3_move) / float(p3_magnitudeAxis);

		rgb.r = (A.mRGB.r * p1 + B.mRGB.r * p2 + C.mRGB.r * p3);
		rgb.g = (A.mRGB.g * p1 + B.mRGB.g * p2 + C.mRGB.g * p3);
		rgb.b = (A.mRGB.b * p1 + B.mRGB.b * p2 + C.mRGB.b * p3);

		//cout<<  Move.r  << "   " << Move.g  << "   "<< Move.b <<endl;
	}


	void InterpolationColor( Vertex &Move )
	{
		Line line1(B , C);
		Line line2(C , A);
		Line line3(B , A);

	    Vertex derivativeA = line1.projectPointToLine(A);
		Vertex derivativeB = line2.projectPointToLine(B);
		Vertex derivativeC = line3.projectPointToLine(C);

		//Barycentric coordinate Axis
		Vertex BarycentricAxisA = (derivativeA - A);
		Vertex BarycentricAxisB = (derivativeB - B);
		Vertex BarycentricAxisC = (derivativeC - C);


		// Interpolation Tri
		Int32 p1_move = (derivativeA - Move).dot(BarycentricAxisA); // BarycentricAxisA.length();
		Int32 p2_move = (derivativeB - Move).dot(BarycentricAxisB); // BarycentricAxisB.length();
		Int32 p3_move = (derivativeC - Move).dot(BarycentricAxisC); // BarycentricAxisC.length();

		Int32 p1_magnitudeAxis = (derivativeA - A).dot(BarycentricAxisA); // BarycentricAxisA.length();
		Int32 p2_magnitudeAxis = (derivativeB - B).dot(BarycentricAxisB); // BarycentricAxisB.length();
		Int32 p3_magnitudeAxis = (derivativeC - C).dot(BarycentricAxisC); // BarycentricAxisC.length();

		float p1_coefficients = float(p1_move) / float(p1_magnitudeAxis);
		float p2_coefficients = float(p2_move) / float(p2_magnitudeAxis);
		float p3_coefficients = float(p3_move) / float(p3_magnitudeAxis);

		 Int32 color = A.color * p1_coefficients / 1 +
				   	   B.color * p2_coefficients / 1 +
					   C.color * p3_coefficients / 1;

		 Move.color = color;
		// cout<<  Move.color << endl;
	}

	/******************************************/
};




//----------------------------------------------//
void SetPixel(Int32 x, Int32 y, Int32 color)
{
  if ((x < 0) || (x >= SCREEN_WIDTH) ||
      (y < 0) || (y >= SCREEN_HEIGHT))
  {
    return;
  }

  Screen[y][x] = color;
}

void InitConsole(void)
{
	Int32 x, y;
	for (y = 0; y < SCREEN_HEIGHT; y++)
	{
		for (x = 0; x < SCREEN_WIDTH; x++)
		{
			Screen[y][x] = 0;
		}
	}
}


void VisualizeConsole(void)
{
  Int32 x, y;
  for (y = 0; y < SCREEN_HEIGHT; y++)
  {
    for (x = 0; x < SCREEN_WIDTH; x++)
    {
      cout << Screen[x][y];
    }
    cout<<endl;
  }
}




// min X and max X for every horizontal line within the triangle
Int32 ContourX[SCREEN_HEIGHT][2];

#define ABS(x) ((x >= 0) ? x : -x)

// Scans a side of a triangle setting min X and max X in ContourX[][]
// (using the Bresenham's line drawing algorithm).
void ScanLine(Int32 x1, Int32 y1, Int32 x2, Int32 y2)
{
	Int32 sx, sy, dx1, dy1, dx2, dy2, x, y, m, n, k, cnt;

	sx = x2 - x1;
	sy = y2 - y1;

	if (sx > 0) dx1 = 1;
	else if (sx < 0) dx1 = -1;
	else dx1 = 0;

	if (sy > 0) dy1 = 1;
	else if (sy < 0) dy1 = -1;
	else dy1 = 0;

	m = ABS(sx);
	n = ABS(sy);
	dx2 = dx1;
	dy2 = 0;

	if (m < n)
	{
		m = ABS(sy);
		n = ABS(sx);
		dx2 = 0;
		dy2 = dy1;
	}

	x = x1; y = y1;
	cnt = m + 1;
	k = n / 2;

	while (cnt--)
	{
		if ((y >= 0) && (y < SCREEN_HEIGHT))
		{
			if (x < ContourX[y][0]) ContourX[y][0] = x;
			if (x > ContourX[y][1]) ContourX[y][1] = x;
		}

		k += n;
		if (k < m)
		{
			x += dx2;
			y += dy2;
		}
		else
		{
			k -= m;
			x += dx1;
			y += dy1;
		}
	}
}






void TriangleInitInterpolationColorRednerPixel(Triangle tri)
{
	Vertex p0 = tri.A;
	Vertex p1 = tri.B;
	Vertex p2 = tri.C;

	Int32 y;
	for (y = 0; y < SCREEN_HEIGHT; y++)
	{
		ContourX[y][0] = LONG_MAX; // min X
		ContourX[y][1] = LONG_MIN; // max X
	}

  ScanLine(p0.x, p0.y, p1.x, p1.y);
  ScanLine(p1.x, p1.y, p2.x, p2.y);
  ScanLine(p2.x, p2.y, p0.x, p0.y);


  Triangle triangle(p0,p1,p2);

  for (y = 0; y < SCREEN_HEIGHT; y++)
  {
	  if (ContourX[y][1] >= ContourX[y][0])
	  {
		  Int32 x = ContourX[y][0];
		  Int32 len = 1 + ContourX[y][1] - ContourX[y][0];

		  // Can draw a horizontal line instead of individual pixels here
		  while (len--)
		  {
			  Int32 _x = x++;
			  Int32 _y = y;

			  Vertex vtx(_x,_y);
			  triangle.InterpolationColor(vtx);

			  swap(_x,_y);

			  GridCoords[_x][_y].color = vtx.color;
			  SetPixel(  _x ,_y ,  vtx.color );

			  triangle.InterpolationRGBColor( vtx , GridCoords[_x][_y].mRGB );


		  }
	  }
  }
}




void InitilisationGridCoords()
{
	for (int y = 0; y < SCREEN_HEIGHT; y+=1)
	{
		for (int x = 0; x < SCREEN_WIDTH; x+=1)
		{
			GridCoords[y][x].x = x;
			GridCoords[y][x].y = y;
		}
	}
}


void FlushBuff()
{
	for (int y = 0; y < SCREEN_HEIGHT; y+=1)
	{
		for (int x = 0; x < SCREEN_WIDTH; x+=1)
		{
			GridCoords[x][y].mRGB.r = 0;
			GridCoords[x][y].mRGB.g = 0;
			GridCoords[x][y].mRGB.b = 0;

			SetPixel(x,y,0);
		}
	}

}



//---------------   Render  -------------------//
void drawGrid()
{
	glBegin(GL_LINES);
	glColor3f(1,1,1);
	for(int i = 0; i < 64; i += 1)
	{
	    glVertex3f((float)i, 0.0f, 0.0f);
	    glVertex3f((float)i, 64.0f, 0.0f);
	    glVertex3f(0.0f, (float)i, 0.0f);
	    glVertex3f(64.0f, (float)i, 0.0f);
	}
	glEnd();
}



void drawPixel( const Pixel& v )
{
	glPushMatrix();
	glPointSize(7);
	glColor3f( v.mRGB.r , v.mRGB.g , v.mRGB.b);
	glBegin(GL_POINTS);
	   glVertex2f(v.x,v.y);
	glEnd();
	glPopMatrix();
}



void DrawCoordsPointFixed()
{
	glPushMatrix();
	for (int y = 0; y < SCREEN_HEIGHT; y+=1)
	{

		for (int x = 0; x < SCREEN_WIDTH; x+=1)
		{
			//оптимизация потока пиксельного конвейера
			if(GridCoords[x][y].mRGB.r != 0 ||
			   GridCoords[x][y].mRGB.g != 0 ||
			   GridCoords[x][y].mRGB.b != 0 )
			{
				drawPixel( GridCoords[x][y] );
			}
		}
	}
	glPopMatrix();
}


//---------------------------------------------//





//---------------- init atribute --------------------------//

void init(void)
{
	    InitilisationGridCoords();
	    FlushBuff();

		glClearColor(0.0,0.0,0.0,0.0);
		glShadeModel(GL_FLAT);

		srand((unsigned)time(NULL));

		Vertex v0 = Vertex( rand() % SCREEN_WIDTH , rand() % SCREEN_HEIGHT , 2);
		Vertex v1 = Vertex( rand() % SCREEN_WIDTH , rand() % SCREEN_HEIGHT , 3);
		Vertex v2 = Vertex( rand() % SCREEN_WIDTH , rand() % SCREEN_HEIGHT , 4);

		v0.mRGB = cRGB(1,0,0);
		v1.mRGB = cRGB(0,1,0);
		v2.mRGB = cRGB(0,0,1);


		Triangle triangle( v0 , v1 , v2 );
		TriangleInitInterpolationColorRednerPixel( triangle );


}



void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	RenderString( 1 , SCREEN_HEIGHT - 5 ,  "key:: R :: Init Random Triangle => RGB" , cRGB(1,1,1));
	RenderString( 1 , SCREEN_HEIGHT - 10 , "key:: C :: Console Print pikagrama => Color " , cRGB(1,1,1));


	//drawGrid();
	DrawCoordsPointFixed();



	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{

       switch (key)
       {
		   case 27: exit(0); break;

		   case 'r': case 'R':  init();  break;
		   case 'c': case 'C':  VisualizeConsole(); break;

		   default:
			   break;
	   }
}


void reshape(int w, int h)
{
	glViewport(0,0,(GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( 0 , SCREEN_WIDTH , 0 , SCREEN_HEIGHT , -1.0 , 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void Timer(int t)
{
	display();
	glutTimerFunc(t , Timer, 10 );
}


int main(int argc, char **argv)
{
		glutInit(&argc,argv);
		glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
		glutInitWindowSize(500,500);
		glutInitWindowPosition(100,100);
		glutCreateWindow("Inerpolation Triangle");

		init();

		glutKeyboardFunc(keyboard);
		glutDisplayFunc(display);
		glutReshapeFunc(reshape);

		glutTimerFunc(0, Timer, (int) 100.0f / 60.0f);
		glutMainLoop();

		return 0;
}
